﻿using System;

namespace 双指针
{
    public class 轮转数组
    {
        // 给你一个数组，将数组中的元素向右轮转 k 个位置，其中 k 是非负数。

        // 示例 1:
        // 输入: nums = [1,2,3,4,5,6,7], k = 3
        // 输出: [5,6,7,1,2,3,4]
        // 解释:
        // 向右轮转 1 步: [7,1,2,3,4,5,6]
        // 向右轮转 2 步: [6,7,1,2,3,4,5]
        // 向右轮转 3 步: [5,6,7,1,2,3,4]

        public static void Rotate(int[] nums, int k)
        {
            var length = nums.Length;
            var newArr = new int[length];
            for (int i = 0; i < length; ++i)
            {
                newArr[(i + k) % length] = nums[i];
            }

            for (int i = 0; i < length; ++i)
            {
                nums[i] = newArr[i];
            }
        }
    }
}