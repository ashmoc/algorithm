﻿using System.Collections.Generic;
using System.Linq;

namespace 双指针
{
    public class 有序数组的平方
    {
        //给你一个按 非递减顺序 排序的整数数组 nums，返回 每个数字的平方 组成的新数组，要求也按 非递减顺序 排序。

        public static int[] SortedSquares(int[] nums)
        {
            List<int> temp = new List<int>();

            for (int i = 0; i < nums.Length; i++)
            {
                temp.Add(nums[i] * nums[i]);
            }

            return temp.OrderBy(o => o).ToArray();
        }
    }
}